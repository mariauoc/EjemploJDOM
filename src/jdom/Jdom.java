/*
 * Ejemplo con JDOM
 */
package jdom;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

/**
 *
 * @author mfontana
 */
public class Jdom {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            SAXBuilder builder = new SAXBuilder();
            Document doc = builder.build("incidencias.xml");
            Element root = doc.getRootElement();            // Elemento raíz
            System.out.println(root.getName());             // Estamos al nivel de incidencias
            List<Element> hijos = root.getChildren();                // Cogemos los hijos
            // Ahora vamos recorriendo los hijos
            for (Element actual : hijos) {
                System.out.println(actual.getName());       // Estamos al nivel de la incidencia
                System.out.println(actual.getAttributeValue("fechahora"));
                // Cogemos los hijos de cada incidencia
                List<Element> nietos = actual.getChildren();
                for (Element nodo : nietos) {
                    System.out.println(nodo.getName() + ": " + nodo.getValue());
                }
            }
        } catch (JDOMException | IOException ex) {
            Logger.getLogger(Jdom.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
